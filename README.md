# Wie nutze ich das neue PP Mag Template: #

Das neue Template ist in eine Tabellen eingeteilt, welche wiederum in mehrere kleine Untertabellen eingeteilt wurde.
Hierbei ist es wichtig, dass die Struktur nicht verändert werden darf.
Bitte folge der Anleitung, es wird alles erklärt.


# Bilder ersetzen: #

## Header: ##

suche nach: `<!-- Header Background Image -->`

tausche den Link unter: `background-image: url('...')` in den Anführungsstrichen von `url(...)` aus.

## im Content: ##

### rechtes Bild: ###
suche nach: `<!-- Content Pic right -->`

tausche den Link aus und gib dem Bild einen Namen in `alt="..."`

passe die Bildbeschreibung im `<p>` Tag unter dem Bild an.

### linkes Bild ###

suche nach `<!-- Content Pic left -->`

tausche den Link aus und gib dem Bild einen Namen in `alt="..."`

passe die Bildbeschreibung im `<p>` Tag unter dem Bild an.


# Content ersetzen: #

## Content 1 links  ##

### Überschrift ändern ###

suche nach: `<!-- Headline 1 left -->` und `<!-- Headline 1 left 2-->` und teile die Überschrift in 2 Elemente (Beispiel siehe Template)

### Text ändern ###

suche nach: `<!-- Context 1 left -->` und ersetze es mit deinem Content, denke aber daran, dass jeder Text mit einem `<p>` beginnen und einem `</p>` endet.

suche nach: `<!-- Button 1 left -->` und tausche im `<a>` Tag den Link unter `href="..."` aus um dem "Mehr erfahren" Button eine Link zu geben.


## Content 1 rechts  ##

### Überschrift ändern ###

suche nach: `<!-- Headline 1 right -->` und `<!-- Headline 1 right 2-->` und teile die Überschrift in 2 Elemente (Beispiel siehe Template)

### Text ändern ###

suche nach: `<!-- Context 1 right -->` und ersetze es mit deinem Content, denke aber daran, dass jeder Text mit einem `<p>` beginnen und einem `</p>` endet.

suche nach: `<!-- Button 1 right -->` und tausche im `<a>` Tag den Link unter `href="..."` aus um dem "Mehr erfahren" Button eine Link zu geben.


## Content 2 links  ##

### Überschrift ändern ###

suche nach: `<!-- Headline 2 left -->` und `<!-- Headline 2 left 2-->` und teile die Überschrift in 2 Elemente (Beispiel siehe Template)

### Text ändern ###

suche nach: `<!-- Context 2 left -->` und ersetze die Listenelemente zwischen `<li>` und `</li>` an.


## Content 2 rechts  ##

### Überschrift ändern ###

suche nach: `<!-- Headline 2 right -->` und `<!-- Headline 2 right 2-->` und teile die Überschrift in 2 Elemente (Beispiel siehe Template)

### Text ändern ###

suche nach: `<!-- Context 2 right -->` und ersetze es mit deinem Content, denke aber daran, dass jeder Text mit einem `<p>` beginnen und einem `</p>` endet.


## Content 3 rechts  ##

### Überschrift ändern ###

suche nach: `<!-- Headline 3 right -->` und `<!-- Headline 3 right 2-->` und teile die Überschrift in 2 Elemente (Beispiel siehe Template)

### Text ändern ###

suche nach: `<!-- Context 3 right -->` und ersetze es mit deinem Content, denke aber daran, dass jeder Text mit einem `<p>` beginnen und einem `</p>` endet.

suche nach: `<!-- Button 3 left -->` und tausche im `<a>` Tag den Link unter `href="..."` aus um dem "Mehr erfahren" Button eine Link zu geben.
